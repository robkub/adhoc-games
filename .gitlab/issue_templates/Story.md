As a (role) I want (decription what is wanted), so that I (description why I want this)

### Acceptance Criterias

- (what most be there to accept the story)
- ...

<!-- Following sections do not apply everytime and can be deleted -->
### Note

(Additional information/context for the story)

### Possible solution

(Description of a solution to fulfill the ACs)

/relate (#ID)

<!-- Following is not allowed to change -->
/label ~story
