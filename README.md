# Adhoc Games

Play _adhoc_ with anyone.

## License

The software is released under AGPL-3.0-or-later, see [COPYING](COPYING).
